<h1>Installing project locally using Docker</h1>
<p>
	This readme explains how to run this project locally using Docker, follow this guide step-by-step.
</p>
<h2>
	Step 1: Get project clone link
</h2>
<p>
	Clone this project by going to its repository's source and clicking on "Clone" at the top right corner. Copy the link to your clipboard.
</p>
<h2>
	Step 2: Clone project into projects folder
</h2>
<p>
	Open your terminal and navigate to your projects folder. Copy down the cloned link into the terminal and hit enter. The project will now clone into the folder. This can take a few minutes depending on the project size.
</p>
<p>
	<strong>Important:</strong> if cloning a project to use as a starter for a NEW project, for example <a href="https://bitbucket.org/webcommitment/wc-theme/src" target="blank">wc-theme</a>, be sure to DELETE the .git folder. This prevents accidentally overwriting the project's repository. If you are cloning a project on which you are going to work, you should leave .git folder as it is.
</p>
<p>
	If all goes well, you should have the following files/folders in your project directory:
	<ul>
		<li>
			.env
		</li>
		<li>
			.git
		</li>
		<li>
			.gitignore
		</li>
		<li>
			.idea (this is a phpstorm folder which you may not have)
		</li>
		<li>
			docker-compose.yaml
		</li>
		<li>
			uploads.ini
		</li>
		<li>
			wp-content
		</li>
	</ul>
</p>
<h2>
	Step 3: Building the project
</h2>
<p>
	The next step is to turn these development files into usable Wordpress files following the following steps.
	<ol>
		<li>
			Open the file <span style="background-color: yellow">.env</span>	
		</li>
		<li>
			Change the variable after DB_NAME to your project name, for example: DB_NAME=<span style="background-color: yellow">bikestore</span>. <br />
			<u><em>This is an important step, as not changing this variable will result into database errors which are hard to solve and may cause you to start over.</em></u>
		</li>
		<li>
			If using phpstorm, go to your terminal below or open a terminal and navigate to the project root.
		</li>
		<li>
			In your terminal, run: <span style="background-color: yellow">docker-compose up -d</span>
		</li>
		<li>
			Wait a few minutes, make some coffee! Docker needs some time to create all Wordpress files from the Docker image. If you can see all Wordpress files, like <em>wp-config.php</em>, <em>wp-admin</em> folder etc, you are good to go.
		</li>
	</ol>
</p>
<h2>
	Step 4: Running your new project
</h2>
<p>
	If all went well, you can try opening your project in your browser. The main website will use <a href="http://localhost:8000" target="blank">localhost:8000</a>, while phpmyadmin runs on <a href="http://localhost:8080" target="blank">localhost:8080</a>. If the setup is correct, you get Wordpress its installation window and you can begin working on your project.
</p>
<p>
	When working on an existing project, be sure to import the database into your newly created database on <a href="http://localhost:8080" target="blank">localhost:8080</a>.
</p>