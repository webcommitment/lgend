<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */

get_header(); ?>
<article id="main-page">
    <section class="main-content">
                <?php
					    while ( have_posts() ) : the_post();
						    get_template_part( 'template-parts/content', 'page' );
					    endwhile; // End of thex loop.
					    ?>

    </section>
</article>
<?php get_template_part( 'template-parts/content', 'newsletter' );?>

<?php
get_footer();
