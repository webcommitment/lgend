<?php
$division = get_field("pijnpunten");
if ( is_page('ondernemer') ){
	$division_class = 'division__business';
	$svg = 'template-parts/svg-ondernemer';
}
elseif ( is_page('particulier') ){
	$division_class = 'division__private';
	$svg = 'template-parts/svg-particulier';
}
?>
<section id="division" class="division division--project">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 col-sm-6 col-md-6 col-lg-6 text-md-left col-xl-6 mb-4 mb-sm-0">
				<div class="<?php echo $division_class; ?> animate-2">
					<div class="row">
						<div class="col-xl-8">
							<h2><?php echo $division['title']; ?></h2>
							<div class="pb-3 list"><?php echo $division['content']; ?></div>
						</div>
						<div class="col-xl-4">
							<?php get_template_part($svg); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
</section>
