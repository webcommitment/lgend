<?php
if ( is_page('ondernemer') ){
	$category = 'succesverhalen-ondernemer';
}
elseif ( is_page('particulier') ){
	$category = 'succesverhalen-particulier';
}

$args = array(
	'post_type' => 'portfolio',
	'category_name' => $category,
	'posts_per_page' => 2
);

$portfolios = get_posts($args);

$sub_title = get_field('project_about_sub-title');
$title = get_field('project_about_title');
$content = get_field('project_about_content');
$link = get_field('project_about_link');
if ( $portfolios ) : ?>
<section class="home-portfolio-about">
	<div class="container-fluid">
		<div class="row">
			<div class="col">
				<div class="home-portfolio-about__left">
					<div class="home-portfolio-about__portfolio">
						<div class="home-portfolio home-portfolio--project">
							<div class="home-portfolio__items">
								<?php foreach ( $portfolios as $portfolio ) : ?>
									<div class="home-portfolio__item home-portfolio__project-item animate-2">
										<img src="<?php echo get_the_post_thumbnail_url($portfolio); ?>" alt="">
										<a class="primary-btn" href="<?php echo get_the_permalink($portfolio); ?>">
											<?php echo get_the_title($portfolio); ?>
										</a>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>

				<div class="home-portfolio-about__right">
					<div class="home-about">
						<?php if (!empty($sub_title)): ?>
							<h5 class="home-about__sub-title animate-3">
								<?php echo $sub_title; ?>
							</h5>
						<?php endif; ?>

						<?php if (!empty($title)): ?>
							<h2 class="home-about__title animate-4 ">
								<?php echo $title; ?>
							</h2>
						<?php endif; ?>

						<?php if (!empty($content)): ?>
							<div class="home-about__content animate-2 list">
								<?php echo $content; ?>
							</div>
						<?php endif; ?>

						<?php if (!empty($link)): ?>
							<a class="home-about__link primary-btn" href="<?php echo $link['url']; ?>">
								<?php echo $link['title']; ?>
							</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
