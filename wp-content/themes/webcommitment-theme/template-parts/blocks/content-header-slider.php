<?php if (have_rows('page-header-slides')): ?>
    <section class="home-page-header">
        <div id="header-slider" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                <?php
                $j = 0;
                while (have_rows('page-header-slides')) : the_row();
                    $content = get_sub_field('slider_content');
                    $link = get_sub_field('slider_link');
                    $image = get_sub_field('slider_image');
                    ?>
                    <div class="home-page-header__item carousel-item <?php echo($j == 0 ? 'active' : ''); ?>">
                        <div class="home-page-header__item-container">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col">
                                        <div class="home-page-header__content">
                                            <div class="home-page-header__text">
                                                <?php echo $content; ?>
                                            </div>
                                            <?php if ($link) : ?>
                                                <div class="home-page-header__cta">
                                                    <a href="<?php echo $link['url']; ?>"
                                                       class="primary-btn">
                                                        <?php echo $link['title']; ?>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-page-header__slide-image"
                                     style="background-image: url('<?php echo $image['sizes']['wc-page-header'] ?>');"
                                ></div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $j++;
                endwhile; ?>
                <div class="home-page-header__carousel-nav">
                    <a class="carousel-control-prev disable-anim" href="#header-slider" role="button" data-slide="prev">
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next disable-anim" href="#header-slider" role="button" data-slide="next">
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>