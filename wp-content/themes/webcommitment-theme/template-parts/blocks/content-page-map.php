<?php $map = get_field( 'map');?>


<section class="with-img">
    <div class="d-flex row">
        <?php if(!empty ($map)): ?>
        <div class="col-12 col-lg-6 ">
            <div class="with-img__left">
                <div class="list">
                    <?php echo the_content(); ?>
                </div>
                <div class="contact-form">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12">
                                    <?php
                            // if ( ICL_LANGUAGE_CODE == 'en' )
                            echo do_shortcode( '[contact-form-7 id="241" title="Contact formulier"]' ); ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=" col-12 col-lg-6">
            <div class="with-img__right">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 px-0">
                    <div class="with-img__right-map">
                        <?php echo $map;?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
</section>
