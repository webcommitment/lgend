<section id="newsletter" class="newsletter">
    <div class="container-fluid">
        <div class="row">
            <!-- contact info -->
            <div class="col-12 col-lg-12 text-center newsletter__contact">
                <?php if (have_rows('main_ctas', 'option')): ?>
                    <div class="container-fluid">
                        <h3> <?php echo __('Neem contact met ons op', 'webcommitment-theme'); ?> </h3>
                        <div class="newsletter__icons">
                            <?php while (have_rows('main_ctas', 'option')): the_row();
                                $icon = get_sub_field('icon', 'option');
                                $link = get_sub_field('link', 'option');
                                ?>
                                <div class="newsletter__icons-item ">
                                    <a class="" href="<?php echo $link['url']; ?>"
                                       aria-label=" <?php echo $link['title']; ?>">
                                        <div class="contacts-block-small__icon">
                                            <img src="<?php echo $icon['url']; ?>" alt="<?php echo $link['title']; ?>"/>
                                        </div>
                                    </a>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
