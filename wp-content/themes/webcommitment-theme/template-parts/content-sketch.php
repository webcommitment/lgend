<?php

?>

<section class="sketch-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="sketch-content__inner">
                    <div class="sketch-content__image">
                        <img src="<?php echo get_field( 'schets_afbeelding' )['url']; ?>" alt="">
                    </div>
                    <div class="sketch-content__content">
						<?php echo get_field( 'schets_content' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
