<?php
$business = get_field("business");
$businessBtn = get_field("business")['button'];

$private = get_field("private");
$privateBtn = get_field("private")['button'];

?>
<section id="division" class="division">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <!-- business -->
            <div class="col-12 col-sm-6 col-md-6 col-lg-6 text-md-left col-xl-6 mb-4 mb-sm-0">
                <div class="division__business  animate-2">
                <div class="row">
                    <div class="col-xl-8">
                        <h2><?php echo $business['title']; ?></h2>
                        <div class="pb-3 list"><?php echo $business['content']; ?></div>
                        <a class="primary-btn" href="<?php echo $businessBtn['url']; ?>">
                            <?php echo $businessBtn['title']; ?></a>
                    </div>
                    <div class="col-xl-4">
                        <?php get_template_part('template-parts/svg-ondernemer'); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- privat -->
        <div class="col-12 col-sm-6 col-lg-6 text-md-left col-xl-6 ">
            <div class="division__private   animate-2">
                <div class="row">
                    <div class="col-xl-8">
                        <h2><?php echo $private['title']; ?></h2>
                        <div class="pb-3 list"><?php echo $private['content']; ?></div>
                        <a class="primary-btn" href="<?php echo $privateBtn['url']; ?>">
                            <?php echo $privateBtn['title']; ?></a>
                    </div>
                    <div class="col-xl-4">
	                    <?php get_template_part('template-parts/svg-particulier'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
