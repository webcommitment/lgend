<section id="blogarchive" class="page page-blog">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 col-md-12 col-lg-11 col-xl-12">
				<div class="page-parent-intro">
					<?php
					$blog = get_queried_object();
					$content = apply_filters('the_content', $blog->post_content);
					echo $content;
					?>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-12 col-md-12 col-lg-12 col-xl-12">
				<?php get_template_part( 'template-parts/child', 'posts-portfolio' ); ?>
			</div>
		</div>
	</div>
</section>
