<?php
/**
 * Template Name: Page with children
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */

get_header(); ?>
    <article id="front-page" class="project-page">
        <section class="main-content">
			<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/blocks/content', 'header-slider' );
				get_template_part( 'template-parts/content', 'division-project' );
				get_template_part( 'template-parts/content', 'sketch' );
				get_template_part( 'template-parts/child-posts-projects' );
				get_template_part( 'template-parts/content', 'project-portfolio' );
				get_template_part( 'template-parts/content', 'newsletter' );
			endwhile; // End of the loop.
			?>
        </section>
    </article>
<?php
get_footer();
